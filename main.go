package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

var cfg = struct {
	Port          int
	MoviesPerPage uint64
	UserAddr      string
	MovieAddr     string
	PaymentAddr   string
}{
	Port:          8080,
	MoviesPerPage: 1,
	MovieAddr:     "localhost:8081",
	UserAddr:      "localhost:8082",
	PaymentAddr:   "http://localhost:8083",
}

func main() {
	var err error

	log.Println("Connecting to gRPC movie service ...")
	MovieStorageClient, err = NewMovieStorageClient(cfg.MovieAddr)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connecting to gRPC user service ...")
	UserStorageClient, err = NewUserStorageClient(cfg.UserAddr)
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/", MainHandler)
	r.HandleFunc("/user", UserPageHandler)
	r.HandleFunc("/login", LoginHandler)
	r.HandleFunc("/logout", LogoutHandler)
	r.HandleFunc("/movie", MovieHandler)
	r.Use(JWTTokenParseMiddleware)

	// Обработчик статических файлов
	fs := http.FileServer(http.Dir("assets"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs))

	// Настройка шаблонизатора
	SetTemplateDir("html")
	SetTemplateLayout("layout.html")
	AddTemplate("main", "main.html")
	AddTemplate("user", "user.html")
	AddTemplate("login", "login.html")
	AddTemplate("logout", "logout.html")
	AddTemplate("movie", "movie.html")
	err = ParseTemplates()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Starting on port %d", cfg.Port)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(cfg.Port), r))
}
