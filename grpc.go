package main

import (
	"context"
	pb "gb-cinema-web/api"
	"google.golang.org/grpc"
	"net/http"
)

var MovieStorageClient pb.MovieStorageClient

func NewMovieStorageClient(addr string) (pb.MovieStorageClient, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}
	return pb.NewMovieStorageClient(conn), nil
}

var UserStorageClient pb.UserStorageClient

func NewUserStorageClient(addr string) (pb.UserStorageClient, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}
	return pb.NewUserStorageClient(conn), nil
}

func getMoviesCount() uint64 {
	req := &pb.GetMovieCountRequest{}
	resp, err := MovieStorageClient.GetMovieCount(context.Background(), req)
	if err != nil {
		return 0
	}
	return resp.Count
}

func getMovies(r *http.Request) ([]Movie, error) {
	req := &pb.GetMoviesRequest{
		Limit:  cfg.MoviesPerPage,
		Offset: cfg.MoviesPerPage * (getURLParamToInt64(r, "page", 1) - 1),
	}
	resp, err := MovieStorageClient.GetMovies(context.Background(), req)
	if err != nil {
		return nil, err
	}
	var movies []Movie
	for _, movie := range resp.Movies {
		m := Movie{
			ID:       movie.Id,
			Name:     movie.Name,
			Poster:   movie.Poster,
			MovieUrl: movie.Url,
			IsPaid:   movie.IsPaid,
		}
		movies = append(movies, m)
	}
	return movies, nil
}

func getMovieByID(id uint64) (Movie, error) {
	req := &pb.GetMovieByIdRequest{
		MovieId: id,
	}
	resp, err := MovieStorageClient.GetMovieById(context.Background(), req)
	if err != nil {
		return Movie{}, err
	}
	return Movie{
		ID:       resp.Id,
		Name:     resp.Name,
		Poster:   resp.Poster,
		MovieUrl: resp.Url,
		IsPaid:   resp.IsPaid,
	}, nil
}

func authUserByEmailAndPassword(r *http.Request) (usr User, err error) {
	r.ParseForm()

	req := &pb.UserEmailAndPassword{
		Email: r.FormValue("email"),
		Pwd:   r.FormValue("pwd"),
	}
	resp, err := UserStorageClient.SearchUserByEmailAndPassword(context.Background(), req)
	if err != nil {
		return User{}, err
	}
	return User{
		ID:     resp.UserId,
		Name:   resp.Name,
		IsPaid: resp.IsPaid,
		Token:  resp.Token,
	}, nil
}
