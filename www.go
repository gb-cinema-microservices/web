package main

import (
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"strconv"
)

type FrontPage struct {
	Movies   []Movie
	User     User
	PayURL   string
	Error    string
	PagePrev uint64
	PageNext uint64
}

func MainHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	front := FrontPage{}

	front.Movies, err = getMovies(r)
	if err != nil {
		log.Printf("Get movie error: %v", err)
	}

	if jwtUser := r.Context().Value("user"); jwtUser != nil {
		front.User = jwtUser.(User)
		front.PayURL = cfg.PaymentAddr + "/checkout?uid=" + strconv.Itoa(int(front.User.ID))
	}

	curPage := getURLParamToInt64(r, "page", 1)
	moviesTotal := getMoviesCount()
	moviesPerPage := cfg.MoviesPerPage

	if moviesTotal > 0 {
		if curPage > 1 {
			front.PagePrev = curPage - 1
		}
		if curPage*moviesPerPage < moviesTotal {
			front.PageNext = curPage + 1
		}
	}

	RenderTemplate(w, "main", front)
}

func UserPageHandler(w http.ResponseWriter, r *http.Request) {
	front := FrontPage{}

	if jwtUser := r.Context().Value("user"); jwtUser != nil {
		front.User = jwtUser.(User)
		front.PayURL = cfg.PaymentAddr + "/checkout?uid=" + strconv.Itoa(int(front.User.ID))
	}

	RenderTemplate(w, "user", front)
}

func MovieHandler(w http.ResponseWriter, r *http.Request) {
	var front struct {
		User  User
		Movie Movie
	}

	if jwtUser := r.Context().Value("user"); jwtUser != nil {
		front.User = jwtUser.(User)
	}

	movieId := getURLParamToInt64(r, "id", 0)
	log.Println("movieId", movieId)

	var err error
	front.Movie, err = getMovieByID(movieId)
	if err != nil {
		log.Println("getMovieByID error", err)
	}

	RenderTemplate(w, "movie", front)
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	front := FrontPage{}
	var err error
	front.User, err = authUserByEmailAndPassword(r)
	if err != nil {
		RenderTemplate(w, "user", front) // ask to enter login+password again
		return
	}
	front.PayURL = cfg.PaymentAddr + "/checkout?uid=" + strconv.Itoa(int(front.User.ID))

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id":   front.User.ID,
		"user_name": front.User.Name,
		"user_paid": front.User.IsPaid,
	})
	tokenString, err := token.SignedString(tokenSecret)
	if err != nil {
		log.Println("JWT error:", err)
	}
	http.SetCookie(w, &http.Cookie{Name: "token", Value: tokenString, HttpOnly: true})

	RenderTemplate(w, "login", front)
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	front := FrontPage{}
	http.SetCookie(w, &http.Cookie{Name: "token", Value: "", HttpOnly: true, MaxAge: -1})
	RenderTemplate(w, "logout", front)
}

func getURLParamToInt64(r *http.Request, paramName string, defaultValue uint64) uint64 {
	var param int
	paramStr := r.URL.Query().Get(paramName)
	param, err := strconv.Atoi(paramStr)
	if err != nil || param < 1 {
		return defaultValue
	}
	return uint64(param)
}
