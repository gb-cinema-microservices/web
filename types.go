package main

type User struct {
	ID     uint64 `json:"id"`
	Name   string `json:"name"`
	IsPaid bool   `json:"is_paid"`
	Token  string `json:"token"`
}

type Movie struct {
	ID       uint64 `json:"id"`
	Name     string `json:"name"`
	Poster   string `json:"poster"`
	MovieUrl string `json:"movie_url"`
	IsPaid   bool   `json:"is_paid"`
}
