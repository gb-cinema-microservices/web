package main

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
)

var tokenSecret = []byte("48h0fv830489bh0458gyb40845g")

func JWTTokenParseMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenCookie, err := r.Cookie("token")
		if err == nil {
			token, err := jwt.Parse(tokenCookie.Value, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}
				return tokenSecret, nil
			})
			if err == nil {
				if tokenPayload, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
					user := User{
						ID:     uint64(tokenPayload["user_id"].(float64)),
						Name:   tokenPayload["user_name"].(string),
						IsPaid: tokenPayload["user_paid"].(bool),
					}
					nextHttpRequest := r.WithContext(context.WithValue(r.Context(), "user", user))
					next.ServeHTTP(w, nextHttpRequest) // proceed with new context
				} else {
					next.ServeHTTP(w, r) // proceed without changes
				}
			} else {
				next.ServeHTTP(w, r) // proceed without changes
			}
		} else {
			next.ServeHTTP(w, r) // proceed without changes
		}
	})
}
